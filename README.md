# Lists
-------
## Sub-Types
- Linked Lists
- Queues
- Stacks

**Container Toggles**
- Count
- Tail
- Current

**Node Toggles**
- Parent
- Single/Double
- Linear/Circular


# Trees
-------
**Container Toggles**
- Count

**Node Toggles**
- Parent
- Binary/Multi
- Sorted


# Graphs
--------
**Node Toggles**
- Directed
- Weighted
